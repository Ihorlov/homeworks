// Теоретичні питання
// 1. Рекурсія це коли функція визиває сама себе.Цей підхід на практиці виористовується для написання більш лаконічного 
// і зрозумілого коду.

// Завдання

let defaultValue;

function getNumber(){
    let number = prompt("Ener  number:", `${defaultValue}` );
    let convertedNumber = Number(number);
    if (isNaN(convertedNumber) || number === "" || convertedNumber < 0 || !Number.isInteger(convertedNumber)){
        defaultValue = number;
        return false;
    }else{
        return convertedNumber; 
    }
}

function checkNumber(){
    let number;
do{
    number = getNumber();
}while(!number && number !==0 )
return number
}

let userNumber = checkNumber();

function factorial (userNumber){
    let result = 1;
    if(userNumber === 1){
        return result
    }else{
        return result = userNumber * factorial((userNumber - 1));
    }
}

let result = factorial (userNumber);
console.log(result);