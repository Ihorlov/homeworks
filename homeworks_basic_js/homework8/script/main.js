//Теоретитичні питання
// 1. Document Object Model (DOM)- це cтруктура HTML документу де кожний тег є об'єктом, з яким може взаємодіяти Java Script
// 2. innerText повертає чистий текст елемента без синтаксису HTML. innertHTML повертає текст з вкладеними в ноього html тегами
// 3. Звернутися до елемента можна наступним чином: getElementByID, getElementsByTagName, getElementsByClassName, getElementsByName, querySelector, querySelectorAll.
// Зараз найзручніший і найпоширеніший метод це querySelector

// Завдання

let paragraphs = document.getElementsByTagName('p');
for(let elem of paragraphs){
    elem.style.backgroundColor = '#ff0000';
}

let optionsList = document.querySelector('#optionsList');
console.log(optionsList);
console.log(optionsList.parentElement);

let childNodes = optionsList.childNodes
for(let elem of childNodes){
    console.log(elem.nodeName, elem.nodeType)
}

let tP = document.querySelector('#testParagraph');
tP.textContent = 'This is a paragraph'
console.log(tP)

let mhElements = document.querySelector('.main-header').children;
for(elem of mhElements){
    elem.classList.add('nav-item')
}


let st = document.querySelectorAll('.section-title')
for(let elem of st){    
    elem.classList.remove('section-title')
}