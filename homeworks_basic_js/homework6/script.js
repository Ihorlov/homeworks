// Теоретичні питання
// 1.Екранування це спосіб виділення символів для того щоб JS приймав їх не як спецсивол мови а як звичайний символ, який
// порібно відобразити.
// 2. Є три основних способи оголосити функцію:
//      1.Function Declaration - це спосіб, який включає в себе всі атрибути функції це ключове слово function, назва
//       функції і її тіло. function getNumber (){}
//      2.Function expression - це спосіб оголошення функції аналогічний Function Declaration але в цьому випадку у 
//      функції не має назви function (){}
//      3. Є ще arrow function. це скорочени синтаксис, який є зручним коли ми використовуємо call back функцію
//      ()=>
// 3.Hoisting це механізм в JS, який переміщує змінні і функції наверх області видимості до виконання коду.
// В залежності від того як ми огоосили функцію вона по різному читається в JS. При Function Declaration ми можемо
// визвати фунцію в будь якому місці нашого коду(до та після оголошення). Тут якраз JS ініціалізує функцію до виконання коду
//  і у нас до неї є доступ скрізь. У випадку Function expression доступ до изову фунуії є тільки після її оголошення.
// Що до змінних, об'явлених за допомогою let та const, так як і з Function expression потрібно використовувати змінну
// після її оголошення. Якщо точніше, то оголошену змінну без присвоєного значення можна використовувати але її значення
// буде undefind. Тобто змынну потрібно використовувати після її оголошення і ініціалізації.


//  Завдання

function getFirstName(){
    let firstName = prompt("Enter your first nsme:");
    if(firstName === null){
        return false;
    }else{
        return firstName;
    }
}

function getLastName(){
    let lastName = prompt("Enter your last nsme:");
    if(lastName === null){
        return false;
    }else{
        return lastName;
    }
}

function chekedFirstName(){
    let firstName
    do{
        firstName = getFirstName()
    }while(!firstName);

    return firstName
}

function chekedLastName(){
    let lastName
    do{
        lastName = getLastName()
    }while(!lastName);

    return lastName
}

function getBirthday (){
    let birthday = prompt("Enter your birthday date in format dd.mm.yyyy")
    return birthday
}


function createNewUser(){
    let firstName = chekedFirstName();
    let lastName = chekedLastName();
    let birthday = getBirthday();

    let newUser = {
        _firstName: firstName,
        _lastName: lastName,
        login: function getLogin () {
            return this._firstName[0].toLowerCase() + this._lastName.toLowerCase();
        },
        birthday,
        age: function getAge(){
            let now = new Date();
            let birthdayConverted = birthday.split(".");
            let todayDate = new Date(now.getFullYear(), now.getMonth(), now.getDate());
            let userBirthday = new Date(birthdayConverted[2], birthdayConverted[1] - 1, birthdayConverted[0]);
            let userBirthdayСurrentYear = new Date(now.getFullYear(), birthdayConverted[1] - 1, birthdayConverted[0]);
            let age = todayDate.getFullYear() - userBirthday.getFullYear();
            if (todayDate < userBirthdayСurrentYear){
                age = age - 1;
            }else if (todayDate === userBirthdayСurrentYear){
                age = age
            }else{
                age = age
            }
            return age
            
        },
        password: function getPassword(){
            return this._firstName[0].toUpperCase() + this.lastName.toLowerCase() + birthday.split(".")[2]

        }
    }
return newUser
}

let newUser = createNewUser();

 Object.defineProperty (newUser, 'firstName', {
    get(){
        return this._firstName
    },
    set(firstName) {
        this._firstName = chekedFirstName()
    }
}
)

Object.defineProperty (newUser, 'lastName', {
    get(){
        return this._lastName
    },
    set(lastName) {
        this._lastName = chekedLastName()
    }
}
)

 console.log(newUser);
 console.log(newUser.age());
 console.log(newUser.password())


