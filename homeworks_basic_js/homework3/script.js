// Теоретичні питання
//  1. Цикл - це якась дія, яка повторюється. Цикли потрібні коли небхідно виконити якусь дію декілька разів. Наприклад 
        // ми "перебираємо" числа від одного до 10 і виводимо кожен раз число в консоль.
//  2. Існує три основних види циклів:
        // 1.Цикл while працює доки виконуеться задана умова. Наприклад виводимо користувачеві цифри доки вони менші 10.
        // 2.Цикл do..while також працює доки виконуеться задана умова, але на відміну від while він спочатку виконає
        // тіло цикла а потім перевірить умову. while працює навпаки. do..while використовуємо наприклад коли отримуємо від
        // користувача якесь значення а потім вже перевіряємо його.
        // 3. Цикл for працює з заданними параметрами. Наприклад у нас є строгий діапазон, який потрібно "перебрати".
        // Наприклад за його допомогою можна порахувати скільки чисел діляться на 3 в діапазоні від 0 до 100.
//  3. Явне перетворення це коніертація одного типу данних в інший за допомогую спеціфльної функції.
        // Наприклад Number-конвертує в число, String - в строчку. Неявне перетворення це можна сказати що автоматичне
        // перетворення в JS коли використовуються данні різних типів. Наприклад логічний оператор неявно сконвертує дані
        // в true або false

// Завдання

// let userNumber = prompt("Enter your number");
// let convertedUserNumber = Number(userNumber);
// if(convertedUserNumber >= 5){
//     for ( i = 0; i <= convertedUserNumber; i++ ){
//         if(i%5 === 0 && i !==0 ){
//             console.log(i)
//         }
//     }
// }else {
//     console.log("Sorry, no numbers")
// }


// Необов'язкове завдання підвищеної складності

let firstNumber;
let secondNumber;
let m;
let n;

function getFirstNumber(){
    let userNumber = prompt("Enter first number:");
    let converted = Number(userNumber);
    if (isNaN(converted) || userNumber === ""){
        alert("Sorry, wrong number");
        return false;
    }else{
        return converted;
    }
}

function getSecondNumber(){
    let userNumber = prompt("Enter second number:");
    let converted = Number(userNumber);
    if (isNaN(converted) || userNumber === ""){
        alert("Sorry, wrong number");
        return false;
    }else{
        return converted;
    }
}

do{
    firstNumber = getFirstNumber();
}while(!firstNumber && firstNumber !==0 && !Number.isInteger(firstNumber));

do{
    secondNumber = getSecondNumber();
}while(!secondNumber && secondNumber !==0 && !Number.isInteger(secondNumber));

if(firstNumber > secondNumber){
    n = firstNumber;
    m = secondNumber;
}else{
    m = firstNumber;
    n = secondNumber;
}

simpleNumber:
for( i = m; i <= n; i++){
    for (let j = 2; j < i; j++) { 
        if (i % j === 0) 
        continue simpleNumber; 
      }
      console.log(i)
    }


