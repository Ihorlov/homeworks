// Теоретичні питання
// 1. Метод об'єкту це по суті функція, яка створена в середі об'єкту, є значенням його властивісті і працює с данними 
// в середині об'єкту.
// 2. Значенням властивості об'єкту може бути будь який тип даних, але не рекоендується створювати властивости без значень.
// 3. Посилальний тип даних - це тип при якому змінній не присвоюється якесь конкретне значення (Наприклад число чи строчка)
// а в змінній зберігається посилання на комірку в пам'яті де знаходится наш об'єкт. На один і той самий об'єкт може
// посилатися безкінечна кількість змінних.

//  Завдання


function getFirstName(){
    let firstName = prompt("Enter your first nsme:");
    if(firstName === null){
        return false;
    }else{
        return firstName;
    }
}

function getLastName(){
    let lastName = prompt("Enter your last nsme:");
    if(lastName === null){
        return false;
    }else{
        return lastName;
    }
}

function chekedFirstName(){
    let firstName
    do{
        firstName = getFirstName()
    }while(!firstName);

    return firstName
}

function chekedLastName(){
    let lastName
    do{
        lastName = getLastName()
    }while(!lastName);

    return lastName
}


function createNewUser(){
    let firstName = chekedFirstName();
    let lastName = chekedLastName();

    let newUser = {
        _firstName: firstName,
        _lastName: lastName,
        login: function getLogin () {
            return this._firstName[0].toLowerCase() + this._lastName.toLowerCase();
        }
    }
return newUser
}

let newUser = createNewUser();

 Object.defineProperty (newUser, 'firstName', {
    get(){
        return this._firstName
    },
    set(firstName) {
        this._firstName = chekedFirstName()
    }
}
)

Object.defineProperty (newUser, 'lastName', {
    get(){
        return this._lastName
    },
    set(lastName) {
        this._lastName = chekedLastName()
    }
}
)

 console.log(newUser.login())