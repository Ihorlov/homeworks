// Теоретичні питання
// 1. Метод forEach працює як цикл, перебираючи всі елементи масиву і плюс до того застосовє до кожного елементу функцію
// яка була задана в умові.
// 2. Очистити масив можно за допомогою встановлення влвстивості lenght в "0", або ж перезаписати поточний масив на
// новий пустий. Також можна перебрати масив циклом і видалити всі елементи.
// 3. Перевірити чи є змінна масивом можна за допомогою методу Array.isArray().

// Завдання 

// Option 1

// function filterBy(type, ...args){
//     let newArray = []
//     for (let arg of args){
//         if (typeof arg !== type){
//             newArray.push(arg)
//         }
//     }
//     return newArray
// }
//     console.log(filterBy("string", 12, 25, "ertwer", "true", null,  ))


// Option 2 

function filterBy(type, ...args){
    return args.filter(function (arg) {return typeof arg !== type} )
}

console.log(filterBy("string", 12, 25, "ertwer", true, "false", null,))