let defaultValue;

function getNumber(){
    let number = prompt("Ener the number of the Fibonacci number:", `${defaultValue}` );
    let convertedNumber = Number(number);
    if (isNaN(convertedNumber) || number === "" || !Number.isInteger(convertedNumber)){
        defaultValue = number;
        return false;
    }else{
        return convertedNumber; 
    }
}

function checkNumber(){
    let number;
do{
    number = getNumber();
}while(!number && number !==0 )
return number
}


function Fibonacci(number){
    let f0 = 0;
    let f1 = 1;
    if(number > 0){
        for(let i = 2; i <= number; i++){
            let f2 = f0 + f1;
            f0 = f1;
            f1 = f2;
        }
        return f1

    }else {
        for(let i = 0; i >= number; i--){
            let f2 = f1 - f0;
            f1 = f0;
            f0 = f2;
        }
        return f1
    }
}
    
let number = checkNumber();
let fibonacciNumber = Fibonacci(number);
alert(`Your fibonacci number is ${fibonacciNumber}`)

